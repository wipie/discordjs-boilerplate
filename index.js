const Discord = require('discord.js');
const client = new Discord.Client();

// Developpment mode
const DEV_MODE = true;

// Main consts
const VERSION = "0.0.1";
const prefix = "!";

// Debug instance
client.once('ready', () => {
    console.log(Color.LightYellow, "Reconnected to " + client.guilds.cache.size + " servers.")
    console.log('Bot initialized succesfully!');
    console.log(Color.LightMagenta, 'Bot is running...')
    console.log("====================================");
});

/*
    Message Listenning
*/
client.on('message', async message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return; // Prevent bot or not targetted message
    const args = message.content.slice(prefix.length).trim().split(' '); // Split messages so we get argument
    const command = args.shift().toLowerCase(); // We assign command for later use
    
	// Monitoring commands
    if (message.channel instanceof Discord.DMChannel) { console.log(`\x1b[94m[Command Monitoring]\x1b[0m\n\x1b[96m     ${getTime()}\x1b[0m\n\x1b[96m     Server: \x1b[95mPrivate Message \x1b[0m\n\x1b[96m     User: \x1b[95m${message.author.tag}\x1b[0m\n\x1b[96m     Command: \x1b[95m${message.author.lastMessage.content}\x1b[0m`); } else { console.log(`\x1b[94m[Command Monitoring]\x1b[0m\n\x1b[96m     ${getTime()}\x1b[0m\n\x1b[96m     Server: \x1b[95m${message.guild.name} \x1b[0m\n\x1b[96m     User: \x1b[95m${message.author.tag}\x1b[0m\n\x1b[96m     Command: \x1b[95m${message.author.lastMessage.content}\x1b[0m`); }
   
	
	
    switch(command) {
        case 'ping': // Basic ping command
			debug("test");
            message.channel.send("Pong!");
            break;
        case 'roll': // Roll between 1, 100
            message.reply('**Rolling... Praying the RNG god...**')
            .then(msg => {
                msg.delete({ timeout: 1500 });
                setTimeout(function (){
                    message.channel.send(`${ message.author } rolled a ***${ Math.floor(Math.random() * 101) }*** !`);
                  }, 1500);
            });
            break;
		default:
			message.channel.send("The command does not exist");
			break;
    }
})


/*
    Various shits
*/

// Color for console.log DO NOT TOUCH
let Color = {
    Cyan: "\x1b[36m%s\x1b[0m",
    Red: "\x1b[31m%s\x1b[0m",
    Green: "\x1b[32m%s\x1b[0m",
    Yellow: "\x1b[33m%s\x1b[0m",
    Blue: "\x1b[34m%s\x1b[0m",
    Magenta: "\x1b[35m%s\x1b[0m",
    White: "\x1b[37m%s\x1b[0m",
    LightBlue: "\x1b[94m%s\x1b[0m",
    LightGreen: "\x1b[92m%s\x1b[0m",
    LightRed: "\x1b[91m%s\x1b[0m",
    LightYellow: "\x1b[93m%s\x1b[0m",
    LightMagenta: "\x1b[95m%s\x1b[0m",
    LightCyan: "\x1b[96m%s\x1b[0m",
}

// Current time for logging DO NOT TOUCH
function getTime() {
    var date = new Date().toDateString();
    var time = new Date().toLocaleTimeString();
    return date + " " + time;
}

// Debug function DO NOT TOUCH
debug = (object) => {
	console.log("\x1b[31m[DEBUG]:\x1b[1m %o", object);
}

/*
    Monitoring when joining / leaving a server.
*/
client.on("guildCreate", guild => {
    console.log("\x1b[92m[+]\x1b[0m \x1b[94m" + guild.name + "\x1b[0m");
})
client.on("guildDelete", guild => {
    console.log("\x1b[31m[-]\x1b[0m \x1b[94m" + guild.name + "\x1b[0m");
})


// Logging bot
if (DEV_MODE === true) {
    console.clear();
    console.log("====================================");
    console.log(Color.Red, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    console.log("\x1b[31m!!!!\x1b[1m\x1b[33mBot is in developpement mode\x1b[0m\x1b[31m!!!!\x1b[0m");
    console.log(Color.Red, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    console.log(Color.LightGreen,'Logging in...');
    client.login('INSERT DEV TOKEN'); // Token dev!!
    console.log(Color.LightGreen,'Logged succesfully.');
} else {
    console.clear();
    console.log("====================================");
    console.log(Color.LightCyan, "Mode: Production");
    console.log(Color.LightGreen,'Logging in...');
    client.login('INSERT PROD TOKEN'); // Token de connexion du bot
    console.log(Color.LightGreen,'Logged succesfully.');
}
